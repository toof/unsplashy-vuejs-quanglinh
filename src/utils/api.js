import axios from 'axios'
import config from '@/config'

const baseUrl = 'https://api.unsplash.com'
const perPage = 25
const headers = {
  Authorization: `Client-ID ${config.clientId}`
}
const api = axios.create({
  headers
})
const unsplash = {
  getPhotos (page = 1) {
    return api.get(`${baseUrl}/photos?per_page=${perPage}&page=${page}`)
  },
  getUserPhotos (username, page = 1) {
    return api.get(`${baseUrl}/users/${username}/photos?per_page=${perPage}&page=${page}`)
  },
  getCollections (page = 1) {
    return api.get(`${baseUrl}/collections?per_page=${perPage}&page=${page}`)
  },
  getCollectionInfo (page = 1, id) {
    return api.get(`${baseUrl}/collections/${id}?per_page=${perPage}&page=${page}`)
  },
  getCollectionPhotos (page = 1, id) {
    return api.get(`${baseUrl}/collections/${id}/photos?per_page=${perPage}&page=${page}`)
  },
  searchPhotos (page = 1, query) {
    return api.get(`${baseUrl}/search/photos?query=${query}&per_page=${perPage}&page=${page}`)
  },
  searchCollections (page = 1, query) {
    return api.get(`${baseUrl}/search/collections?query=${query}&per_page=${perPage}&page=${page}`)
  }
}
const plugin = {
  install (Vue) {
    Object.defineProperty(Vue.prototype, '$unsplash', {
      get () {
        return unsplash
      }
    })
  }
}

export default plugin
