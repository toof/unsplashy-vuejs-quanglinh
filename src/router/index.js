import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home'

import Collection from '@/views/Collection'
import CollectionPhotos from '@/views/CollectionPhotos'

import SearchPhotos from '@/views/SearchPhotos'
import SearchCollections from '@/views/SearchCollections'

import User from '@/views/User'
import UserPhotos from '@/views/UserPhotos'
import Login from '@/views/Login'
import Register from '@/views/Register'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/user',
      name: 'user',
      component: User,
      children: [
        {
          path: 'login',
          name: 'user:login',
          component: Login
        },
        {
          path: 'register',
          name: 'user:register',
          component: Register
        }
      ]
    },
    {
      path: '/collections',
      component: Collection
    },
    {
      path: '/photos/:username',
      component: UserPhotos
    },
    {
      path: '/collections/:collectionId',
      component: CollectionPhotos
    },
    {
      path: '/search/photos/:query',
      component: SearchPhotos
    },
    {
      path: '/search/collections/:query',
      component: SearchCollections
    }
  ]
})
