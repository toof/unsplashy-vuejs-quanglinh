const mixin = {
  data () {
    return {
      currentPage: 1
    }
  },
  methods: {
    updateCurrentPage () {
      this.currentPage++
    }
  }
}

export default mixin
