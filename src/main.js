import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import 'bootstrap/dist/css/bootstrap.css'
import api from '@/utils/api'
import vmodal from 'vue-js-modal'

Vue.use(vmodal)
Vue.use(api)
Vue.config.productionTip = false

export default new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
